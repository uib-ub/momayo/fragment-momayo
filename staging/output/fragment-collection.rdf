<?xml version="1.0" encoding="utf-8" ?>
<rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
         xmlns:dct="http://purl.org/dc/terms/"
         xmlns:mmo="http://purl.org/momayo/mmo/"
         xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
         xmlns:skos="http://www.w3.org/2004/02/skos/core#"
         xmlns:foaf="http://xmlns.com/foaf/0.1/"
         xmlns:ecrm="http://erlangen-crm.org/current/"
         xmlns:edm="http://www.europeana.eu/schemas/edm/"
         xmlns:void="http://rdfs.org/ns/void#">

<void:Dataset rdf:about="http://data.ub.uib.no/dataset/id/7bef54b7-4826-49c5-beb8-0ca0f5da0193">
    <dct:title>Dataset - From manuscript fragments to book history</dct:title>
    <foaf:homepage rdf:resource="http://melod.uib.no/id/51ca2d77-9baf-4ba6-8ba5-c662589ba25b"/>
    <void:feature rdf:resource="http://www.w3.org/ns/formats/RDF_XML"/>
    <void:sparqlEndpoint rdf:resource="https://sparql.ub.uib.no/melod/sparql"/>
    <foaf:page rdf:resource="https://git.app.uib.no/uib-ub/momayo/fragment-momayo/raw/master/data/data.rdf-xml.owl"/>
  </void:Dataset>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/fa998d2b-f490-4d64-856b-104c362baaa3">
    <rdf:type rdf:resource="http://purl.org/momayo/mmo/Aggregation"/>
    <mmo:uuid>fa998d2b-f490-4d64-856b-104c362baaa3</mmo:uuid>
    <dct:title xml:lang="en">Latin manuscript fragments in norwegian collections</dct:title>
    <ecrm:P109_has_current_or_former_curator rdf:resource="http://data.ub.uib.no/id/51ca2d77-9baf-4ba6-8ba5-c662589ba25b"/>
    <mmo:hasServiceDocument rdf:resource="http://data.ub.uib.no/id/bea87277-fe2d-44ea-a46e-3e265b0d00e0"/>
    <dct:description xml:lang="en">This collection gathers the separate fragments found in Norway. At the moment all the fragments are held by the National Archive of Norway.</dct:description>
  </rdf:Description>

  <mmo:Aggregation rdf:about="http://data.ub.uib.no/id/e41c70fe-c840-402b-9e11-b6558e5574b0">
    <mmo:uuid>e41c70fe-c840-402b-9e11-b6558e5574b0</mmo:uuid>
    <dct:title xml:lang="en">Reconstructed codices from Norway</dct:title>
    <ecrm:P109_has_current_or_former_curator rdf:resource="http://data.ub.uib.no/id/51ca2d77-9baf-4ba6-8ba5-c662589ba25b"/>
    <mmo:hasServiceDocument rdf:resource="http://data.ub.uib.no/id/bea87277-fe2d-44ea-a46e-3e265b0d00e0"/>
    <dct:description xml:lang="en">This collection gathers reconstructed manuscripts identified by scholars and by the project &quot;From manuscript fragments to book history&quot;.</dct:description>
  </mmo:Aggregation>

  <ecrm:E74_Group rdf:about="http://data.ub.uib.no/id/51ca2d77-9baf-4ba6-8ba5-c662589ba25b">
    <foaf:logo>https://data.ub.uib.no/img/fragmentlogo.png</foaf:logo>
    <mmo:uuid>51ca2d77-9baf-4ba6-8ba5-c662589ba25b</mmo:uuid>
    <mmo:hasActorType>
      <mmo:ActorType rdf:about="http://data.ub.uib.no/id/8e08dcee-c70f-401b-a406-955c9b84eb45">
        <mmo:uuid>8e08dcee-c70f-401b-a406-955c9b84eb45</mmo:uuid>
        <skos:prefLabel xml:lang="no">Prosjekt</skos:prefLabel>
        <skos:prefLabel xml:lang="en">Project</skos:prefLabel>
      </mmo:ActorType>
    </mmo:hasActorType>

    <foaf:name xml:lang="en">From manuscript fragments to book history</foaf:name>
    <foaf:homepage>
      <foaf:Domument rdf:about="https://www.uib.no/en/rg/manuscript_fragments">
        <rdfs:label xml:lang="en">From manuscript fragments to book history - uib.no</rdfs:label>
      </foaf:Domument>
    </foaf:homepage>

    <foaf:page>
      <foaf:Domument rdf:about="http://fragment.uib.no/">
        <rdfs:label xml:lang="en">Virtual manuscripts</rdfs:label>
      </foaf:Domument>
    </foaf:page>

    <ecrm:P109i_is_current_or_former_curator_of rdf:resource="http://data.ub.uib.no/id/fa998d2b-f490-4d64-856b-104c362baaa3"/>
    <ecrm:P109i_is_current_or_former_curator_of rdf:resource="http://data.ub.uib.no/id/e41c70fe-c840-402b-9e11-b6558e5574b0"/>
    <ecrm:P107_has_current_or_former_member rdf:resource="http://data.ub.uib.no/id/1e6a57a55daafd8ac27f7eb6188926425fdb0253"/>
    <ecrm:P107_has_current_or_former_member rdf:resource="http://data.ub.uib.no/id/76e8daf43b51c7469b266d37aab6e3ed52f679bc"/>
    <ecrm:P107_has_current_or_former_member rdf:resource="http://data.ub.uib.no/id/bd62be75218df6c5083b334231b86e13ce40799b"/>
    <dct:description xml:lang="en">Medieval manuscript fragments are some of Norways most significant testimonies to medieval book history. Through analysis of a selection  of the thousands of manuscript fragments kept in public collections, this project explored the first centuries of book and scribal culture in Norway. The project ended in May 2017, but the research continues. The focus of the project was primarily on three aspects of Norwegian book culture: The impact of regions and centres in Western Europe, with an emphasis on England (primarily the south-east, including London) and France (primarily the north-west, including Paris). The effect of Nordic ecclesiastical collaboration, particularly with Denmark (Lund) in the twelfth and Iceland in the thirteenth century. The development of book producing centres in Norway, and how local scriptoria adapted international impulses in the twelfth and thirteenth centuries.</dct:description>
  </ecrm:E74_Group>

  <mmo:EventType rdf:about="http://data.ub.uib.no/id/78dfb070-c3d9-4fdd-a66e-bae313b2443e">
    <mmo:uuid>78dfb070-c3d9-4fdd-a66e-bae313b2443e</mmo:uuid>
    <skos:prefLabel xml:lang="no">Oppstykking</skos:prefLabel>
    <skos:prefLabel xml:lang="en">Dismemberment</skos:prefLabel>
  </mmo:EventType>

  <mmo:ServiceDocument rdf:about="http://data.ub.uib.no/id/bea87277-fe2d-44ea-a46e-3e265b0d00e0">
    <mmo:uuid>bea87277-fe2d-44ea-a46e-3e265b0d00e0</mmo:uuid>
    <rdfs:label xml:lang="en">Service domument for &quot;Latin manuscript fragments in norwegian collections&quot;.</rdfs:label>
    <mmo:isServiceDocumentOf rdf:resource="http://data.ub.uib.no/id/e41c70fe-c840-402b-9e11-b6558e5574b0"/>
    <mmo:isServiceDocumentOf rdf:resource="http://data.ub.uib.no/id/fa998d2b-f490-4d64-856b-104c362baaa3"/>
    <mmo:hasSearchIndex>melod</mmo:hasSearchIndex>
    <mmo:hasSearchService>melod</mmo:hasSearchService>
  </mmo:ServiceDocument>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/fe466483-0e44-4911-8b1b-a93e01a3e3d5">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/1e0f5d7b-1e57-4bd8-a0fc-f99dfc9c3e51">
        <rdfs:label xml:lang="en">The Saint Alphegus Missal</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header14725</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/c9af7804-394d-409d-afd7-934914c25af7">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/5de193b3-990c-4b55-a1eb-49c2d274dbb8">
        <rdfs:label xml:lang="en">The Nordland Missal</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header14772</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/5cc583d7-e66f-405f-92e4-89b2189a37f2">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/2a9b8604-7eeb-40a9-abc8-254e6d9e064b">
        <rdfs:label xml:lang="en">The Easter Sequence Missal</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header15544</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/e2d5d974-7db2-4317-ba13-58383f7d69b1">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/5fa11e71-ed86-42da-b1ec-d0643f7d7864">
        <rdfs:label xml:lang="en">The Good Friday Missal (Mi 12)</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header16684</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/f64bd815-43ee-4f47-91a3-c920ea4743c8">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/b5e3dcfb-5d91-4db0-a7e2-9e1f7ef2c82d">
        <rdfs:label xml:lang="en">The Tønsberg Breviary</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header15542</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/fe01cede-e2b8-4ef8-a81b-c0dacaa70fb6">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/5b81cb19-6451-496c-b3d0-bdcfc415ad9f">
        <rdfs:label xml:lang="en">The Saint Edmund Antiphoner</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header15545</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/47d7b181-44d0-4d72-94a2-05fdcc94e35f">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/eb7d8afb-db0e-45da-a5aa-7cde23b561f5">
        <rdfs:label xml:lang="en">The Saint Lucia Antiphoner</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header15546</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/6307432b-7848-4033-8760-4ee47049adbb">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/edfd1cbc-db99-4922-a8ed-e7c430762bad">
        <rdfs:label xml:lang="en">An antiphoner from Nidaros</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header16121</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/57fb6327-2720-49b5-8b77-e95bf5451e50">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/40f9a62f-dc87-403e-96fb-d76225c14e14">
        <rdfs:label xml:lang="en">A Flemish Antiphoner</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header16685</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/e04c20f6-aa5b-4106-9f52-77861d5669b9">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/373b3a28-0e7e-4049-b8c8-96d27b414fbc">
        <rdfs:label xml:lang="en">The Sogn Psalter</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header14816</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/046f9a4c-9c58-4e05-911b-c91b4cb8188d">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/4c4dfcbd-b428-4a19-a867-a07046d87ca6">
        <rdfs:label xml:lang="en">The Reused Psalter</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header14849</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/e7c74bba-030d-433a-8c9b-2ee5ead58f61">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/201d11a7-ffb2-4280-9232-f362293abb72">
        <rdfs:label xml:lang="en">The Passiontide Lectionary</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header14818</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/292257da-7387-43fb-a7ad-184ebbb1e22c">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/39782c6d-d4f8-4468-9328-663ca1dbed93">
        <rdfs:label xml:lang="en">The Icelandic Breviary-Missal</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header15543</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/1b2ee6a4-5b91-47ee-87e4-9dfc1c01531a">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/1db23c5c-27e8-43d0-b429-c1956c1cc949">
        <rdfs:label xml:lang="en">A Northern French Legendary in Lund</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header16680</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

  <rdf:Description rdf:about="http://data.ub.uib.no/id/ed9f3c25-65ed-404f-92c7-b77f5c2b54a4">
    <edm:isShownAt>
      <mmo:WebResource rdf:about="http://data.ub.uib.no/id/6bdde773-22bc-43b3-83ea-dadf10053bab">
        <rdfs:label xml:lang="en">The Saint Olaf Sequentiary</rdfs:label>
        <foaf:page>http://fragment.uib.no/?k=4643#header16788</foaf:page>
      </mmo:WebResource>
    </edm:isShownAt>

  </rdf:Description>

</rdf:RDF>