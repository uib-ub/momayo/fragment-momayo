# Fragmentbasen

> her kjem Excel-arket og nokre biletfiler. Me er førebudde på at det er utfordringar med dette. Synnøve har heldigvis to månader att om noko må gjerast annleis (me er til dømes førebudde på at namna på fotofilene kanskje må fysisk førast opp i lista – men det er ikkje noko problem, det berre tek litt tid).  
> 
> Eg ser også at me må ha ei visningsform på bestandssignaturen som er litt annleis, og som fylgjer tidlegare standard («Lat. fragm. 1, 1-13»). Me får sjå korleis dette best skal gjerast og korleis det skal førast... I morgon gjeld vel å få på plass det viktigaste og ein tidsplan. Eg skal også sjekke finansane.  
> 
> Venleg helsing Åslaug

Diagram for transformasjonene fragmentene har vært en del av.

## Installering

Vi kjører den nye "Momayo-modellen", med submodules for forms og ontologi. Det vil si at de viktigste filene er:

* data/data.rdf-xml.owl
* form-momayo/form-momayo.pprj
* form-momayo/form-momayo.repository
* onto-momayo/mmo.owl

Resten er knyttet til konverteringen til RDF og UB sin CIDOC-CRM-baserte datamodell.

*Dette repoet benytter Git LFS!*

```bash
git clone git@git.app.uib.no:uib-ub/momayo/fragment-momayo.git
git submodule init
git submodule update

# Run build.xml after configuring local.properties.default > local.properties
```

## Dokumentasjon

Oppgaven består i å ta et regneark med alle fragment hos Arkiververket (lokalisert i Oslo). Dette arkivet har mistet sin originale ordning, så det er viktig å vise hvordan det var organisert.

Arbeidet skjer direkte i Excel av Åslaug Ommundsen og Synnøve Myking

Det er ca 60gb med filer med. Alle fragment er digitalisert.

Filene har samme filnavn som `Identifier`.

*UB kan*

* Konvertere regneark til RDF og gjøre data tilgjengelig via et Sparql Endpoint
* Koble poster i regneark til de digitaliserte fragmentene
* (Eventuelt gjøre data tilgjengelig for redigering)
* (Eventuelt gjøre data tilgjengelig i marcus.uib.no

*Fragmentbasen må*

* Levere oppdatert og fullført regneark (under konvertering kan data ikke endres ut over datavasking ledet av DST)
* Levere alle filer som skal kobles til på ekstern HD eller annet måte som er enkel for DST å komme til

*Estimert tidsbruk:* et månedsverk

Data versjoneres her: https://gitlab.com/ubbdev/fragment-data (privat repositorie)

## Relaterte nettsider
Fremtidig prosjekt å sy dette sammen

### fragment.uib.no
* http://fragment.uib.no/

### Gammel side laget av Øyvind:
* http://ub.uib.no/fragment/list/index.html

## Felt i regneark

### Identifier
Bestandssignatur. De fleste begynner med NRA_Lf, NRA_U eller NRA_B. Noen har bare et nummer som identifikator (blir ryddet).

```
NRA             # Arkivverket
 -0001          # Representerer ett org. dokument (*)
  -10203        # Hvilke fragment man har (01, 02 og 03)
   -a           # Dersom det er flere avbildninger
```

### Number of fragments
Data: integer

Mellom 1-58 fragment pr. konvolutt

### Account provenance
Steder skilt med "||".

Fragmentene ble brukt til å forsterke skattelister og annet. Hvilket sted listen omhandlet er lagt her. Henger sammen med *Start year* og *End year*.

### Start year
Årstall. Se *Account provenance*

* En rad med "ca. 1540"

### End year
Årstall. Se *Account provenance*.

* Én rad med "ca. 1540"
* én med 1695 [sic]
* én med 1716 (error?)

### Author
Historiske personer

### Title
Ser fine ut

### Genre
For eksempel "Breviary". Noen med spørsmål, noen med "or", en dobbel skilt med "||", noen med "(SM)". SM er initial på den som påstår at det er den *Genre*

Må vaskes

### Gjerløw regristration
ID fra annet register? Noen som er utvetydige, "Br 43 (NRA_Lf_1132)"

### Origin
Land. Mange usikre. Best egnet som tekst.

### Century
Blanding av romertall og tall. Mange er usikre, spørsmåltegn, spenn og in., med., ex. 

### Notes
Blanding av informasjon om materiale, hvem som har skrevet det, relasjoner til andre fragment. etc. 

Bør vaskes og splittes i flere kolonner?

### Same ms as
Mange ID'er.

### Literature
Enkle referanser. "Etternavn, ÅÅÅÅ:sidetall"

## Avklaringer

### Accession number
Her har vi NRA_LF, NRA_Ux, NRA_Bx, NRA_ls_RK_LR_Bgh, NRA_la_RK_LR_Lgs, NRA_la_RK_LR_Svg, NRA_la_RK_LR_Trh og NRA_la_RK_LR_Vkl. Dersom jeg skal lage en overordnet post på det originale dokumentet og vi har sameAs relasjoner fra "From the same manuscript as", er det en brukbar ID i signaturene? NRA_Lf er ok.

"From the same manuscript as" ser ut til at har blitt benyttet til å indikere at fragment skal flyttes.

Eksempel: http://127.0.0.1:3333/project?project=2404124070453&ui=%7B%22facets%22%3A%5B%7B%22c%22%3A%7B%22type%22%3A%22list%22%2C%22name%22%3A%22Accession%20number%20%28Bestandssignatur%29%22%2C%22columnName%22%3A%22Accession%20number%20%28Bestandssignatur%29%22%2C%22expression%22%3A%22facetCount%28value%2C%20%27value%27%2C%20%27Accession%20number%20%28Bestandssignatur%29%27%29%20%3E%201%22%2C%22omitBlank%22%3Afalse%2C%22omitError%22%3Afalse%2C%22selectBlank%22%3Afalse%2C%22selectError%22%3Afalse%2C%22invert%22%3Afalse%7D%2C%22o%22%3A%7B%22sort%22%3A%22name%22%7D%2C%22s%22%3A%5B%5D%7D%2C%7B%22c%22%3A%7B%22type%22%3A%22list%22%2C%22name%22%3A%22Accession%20number%20%28Bestandssignatur%29%22%2C%22columnName%22%3A%22Accession%20number%20%28Bestandssignatur%29%22%2C%22expression%22%3A%22value%22%2C%22omitBlank%22%3Afalse%2C%22omitError%22%3Afalse%2C%22selectBlank%22%3Afalse%2C%22selectError%22%3Afalse%2C%22invert%22%3Afalse%7D%2C%22o%22%3A%7B%22sort%22%3A%22name%22%7D%2C%22s%22%3A%5B%5D%7D%2C%7B%22c%22%3A%7B%22type%22%3A%22text%22%2C%22name%22%3A%22From%20the%20same%20manuscript%20as%22%2C%22columnName%22%3A%22From%20the%20same%20manuscript%20as%22%2C%22mode%22%3A%22text%22%2C%22caseSensitive%22%3Afalse%2C%22query%22%3A%22NRA_Lf_%22%7D%7D%5D%7D
