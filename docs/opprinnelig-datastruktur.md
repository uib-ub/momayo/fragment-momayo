# Opprinnelig datastruktur

```
{
"Accession number (Bestandssignatur)" : "NRA_Lf_0001_01020304050607080910111213",
"Number of fragments in envelope" : 13,
"Account provenance" : "Trondheim || Jemtland & Herjedalen || Herjedalen || Senja || Helgeland || Strinda",
"Start year" : 1623,
"End year" : 1638,
"Author" : "Isidore of Seville",
"Title" : "Etymologiae",
"Genre" : "Encyclopedia",
"Gjerløw's registration" : "Auctores",
"Origin" : "Norway or France",
"Date" : "xii med.",
"Physical characteristics" : "Initials red, green",
"Notes" : null,
"From the same manuscript as" : "NRA_Lf_0002_0102030405060708 || NRA_Lf_0003_01020304050607080910",
"Literature" : "Ommundsen 2008 || Karlsen 2013:229-30"
}
```

```
{
"Accession number (Bestandssignatur)" : "NRA_Lf_0001_01020304050607080910111213",
"Number of fragments in envelope" : 13,
"Account provenance" : "Trondheim || Jemtland & Herjedalen || Herjedalen || Senja || Helgeland || Strinda",
"Start year" : 1623,
"End year" : 1638,
"Author" : "Isidore of Seville",
"Title" : "Etymologiae",
"Genre" : "Encyclopedia",
"Gjerløw's registration" : "Auctores",
"Origin" : "Norway or France",
"Date" : "xii med.",
"Physical characteristics" : "Initials red, green",
"Notes" : null,
"From the same manuscript as" : "NRA_Lf_0002_0102030405060708 || NRA_Lf_0003_01020304050607080910",
"Literature" : "Ommundsen 2008 || Karlsen 2013:229-30"
}
```